FROM ubuntu:bionic-20200713

MAINTAINER root@cri.epita.fr

ENV TRACKER_PORT=8000

RUN apt update && apt install -y bittorrent

RUN groupadd bttrack && useradd --no-log-init --system --no-create-home -g bttrack bttrack

RUN mkdir -p -m '0755' '/var/lib/bittorrent' && chown bttrack:bttrack '/var/lib/bittorrent'

USER bttrack:bttrack

EXPOSE $TRACKER_PORT/tcp

CMD ["sh", "-c", "/usr/bin/bttrack --port $TRACKER_PORT --dfile /var/lib/bittorrent/dfile"]
