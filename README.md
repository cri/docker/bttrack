# Bittorrent

This image of bittorrent is running on a debian based container.

It exposes the port `8000` by default.

# Run

```shell
docker run -p 8000:8000 <container>
```

# Testing

- generate a `.torrent` (`mktorrent` is needed for this step):
```shell
TRACKER_PORT="8000"
ANNOUNCE_URL="http://localhost:${TRACKER_PORT}/announce"
FILE="Dockerfile"
rm "/tmp/${FILE}.torrent"
mktorrent -a "${ANNOUNCE_URL}" -o "/tmp/${FILE}.torrent" "${FILE}"
```
- run the docker container
- add the newly created .torrent file to a torrent client (ex: `transmission-gui`)
- check if the tracker is properly responding to the torrent client's sollicitations
